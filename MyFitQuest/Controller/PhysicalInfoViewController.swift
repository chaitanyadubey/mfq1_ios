//  
//  PhysicalInfoViewController.swift
//  MyFitQuest
//  Using Swift 4.0
//
//  Created by Abhishek Singh on 26/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import UIKit
import JGProgressHUD

class PhysicalInfoViewController: UIViewController, HUDProtocol {
    // MARK: - Instance variables
    var instance: JGProgressHUD = JGProgressHUD(style: .extraLight)
    var currentMember: Member?
    
    
    // MARK: - Life cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    // MARK: - user Defined button Action
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
