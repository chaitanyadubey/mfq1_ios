//
//  HomeViewController.swift
//  MyFitQuest
//
//  Created by Imac on 13/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import UIKit
import JGProgressHUD

class HomeViewController: UIViewController, HUDProtocol {
    // MARK: - Instance variables
    var instance: JGProgressHUD = JGProgressHUD(style: .extraLight)
    var memberId: String?
    var currentMember: Member?
    
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var enrolledLabel: UILabel!
    
    // MARK: - Life cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateUI()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        profileImageView.addGestureRecognizer(tap)
        profileImageView.isUserInteractionEnabled = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getMember()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    // MARK: - user Defined button Action
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        guard let _ = currentMember else { return }
        performSegue(withIdentifier: "MyProfile", sender: self)
    }
    
    func updateUI() {
        guard let member = currentMember else {
            return
        }
        if let url = member.avatar {
            profileImageView.setImageWith(URL: url, placeHolder: nil)
            profileImageView.backgroundColor = UIColor.gray
        } else {
            profileImageView.backgroundColor = UIColor.gray
        }
        nameLabel.text = "\(member.firstname) \(member.lastname)"
        locationLabel.text = "\(member.city ?? "") \(member.state ?? "")"
        if let date = member.signupdate {
            enrolledLabel.text = "Enrolled \(DateFormatter.MMMdyyyy.string(from: date))"
        } else {
            enrolledLabel.text = ""
        }
    }
    
    @IBAction func signOutAction(_ sender: Any) {
        Member.current = nil
        if let count = navigationController?.viewControllers.count, count > 1 {
            navigationController?.popToRootViewController(animated: true)
        } else {
            guard let viewController = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: "LoginController") as? LoginViewController else { return }
            navigationController?.viewControllers = [viewController]
        }
    }
    
    @IBAction func updatePasswordAction(_ sender: Any) {
        guard let mid = memberId else {
            return
        }
        guard let oldPassword = oldPasswordField.text, oldPassword.count > 0 else {
            showAlert(message: "Password can't be blank")
            return
        }
        guard let newPassword = newPasswordField.text, newPassword.count > 0 else {
            showAlert(message: "New password can't be blank")
            return
        }
        let accountUpdate = AccountDetailUpdate(mid: mid, password: newPassword, old_password: oldPassword)
        updatePassword(requestObject: accountUpdate)
    }
    
    
    // MARK: - API
    func getMember() {
        guard let memberid = memberId else { return }
        //showLoader()
        FitQuestAPI.shared.getMemberDetail(Parameters: ["mid": memberid]) {[weak self] (response) in
            self?.hideLoader()
            self?.parseAPIResponse(response, type: UserDetail.self, completion: { (userDetail) in
                if userDetail.status == "success" {
                    if UserDetail.remember {
                        Member.current = userDetail.member
                    }
                    self?.currentMember = userDetail.member
                    self?.updateUI()
                }
            })
        }
    }
    
    func updatePassword(requestObject: AccountDetailUpdate) {
        do {
            guard let paramaters = try Utilities.encode(requestObject) else { return }
            showLoader()
            FitQuestAPI.shared.updateMemberAccountDetail(Parameters: paramaters) {[weak self] (response) in
                self?.hideLoader()
                self?.parseAPIResponse(response, type: FQAPIResponse.self, completion: { (resp) in
                    if resp.status == "success" {
                        self?.showAlert(message: "Password updated.")
                        self?.oldPasswordField.text = nil
                        self?.newPasswordField.text = nil
                    }
                })
            }
        } catch let error {
            self.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let viewController = segue.destination as? MyPictureViewController {
            viewController.currentMember = currentMember
        }
    }
 

}

