//
//  RegisterViewController.swift
//  MyFitQuest
//
//  Created by Imac on 14/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import UIKit
import JGProgressHUD

class RegisterViewController: UIViewController, HUDProtocol, UITextFieldDelegate {
    // MARK: - Instance variables
    var instance: JGProgressHUD = JGProgressHUD(style: .extraLight)
    var countries:[Country] = [Country]()
    var selectedCountry: Country?
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    
    // MARK: - Life cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCountryList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    // MARK: - user Defined button Action
    
    func resetFields() {
        emailField.text = ""
        passwordField.text = ""
        confirmPasswordField.text = ""
        countryField.text = ""
    }
    
    func showCountrySelector() {
        //Country cell
        let countrySelector = CountrySelectionInterfaceController()
        _ = countrySelector.view
        countrySelector.sortCustomArray(countries)
        countrySelector.completionBlock = {[weak self] (countryname, countryId) in
            guard let country = countryname  else { return }
            self?.countryField.text = country
            if countryname != nil && countryId != nil {
                self?.selectedCountry = Country(name: countryname!, id: countryId!)
            }
        }
        self.navigationController?.present(UINavigationController(rootViewController: countrySelector), animated: true, completion: nil)
        
    }
    
    @IBAction func countryAction(_ sender: Any) {
        showCountrySelector()
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        guard let email = emailField.text, Utilities.isValidEmail(email)  else {
            showAlert(message: "Invalid Email")
            return
        }
        guard let password = passwordField.text, password.count > 0 else {
            showAlert(message: "Password can't be blank")
            return
        }
        guard let cPassword = confirmPasswordField.text, password == cPassword else {
            showAlert(message: "Password not matched")
            return
        }
        guard let country = selectedCountry else {
            showAlert(message: "Country can't be blank")
            return
        }
        
        let registerRequest = RegisterRequest(uname: email, password: password, country: country.id, manufacturerid: "5")
        register(requestObject: registerRequest)
    }
    
    @IBAction func guestAction(_ sender: Any) {
        
    }
    
    // MARK: - Text field delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryField {
            textField.resignFirstResponder()
            showCountrySelector()
        }
    }
    
    // MARK: - API
    func getCountryList() {
        showLoader()
        FitQuestAPI.shared.countryList { [weak self] (response) in
            self?.hideLoader()
            self?.parseAPIResponse(response, type: CountryList.self, completion: { (resp) in
                print(resp.countries.count)
                self?.countries = resp.countries
            })
        }
    }
    
    func register(requestObject: RegisterRequest) {
        do {
            guard let paramaters = try Utilities.encode(requestObject) else { return }
            showLoader()
            FitQuestAPI.shared.registerWith(Parameters: paramaters) {[weak self] (response) in
                self?.hideLoader()
                self?.parseAPIResponse(response, type: FQAPIResponse.self, completion: { (resp) in
                    if resp.status == "success-confirmemail", let email = paramaters["uname"]{
                        let message = "Verification mail has been sent to \(email)"
                        self?.showAlert(title: "MyFitQuest", message: message, dismissTitle: "Ok", dismissAction: {
                            self?.resetFields()
                        })
                    }
                }, errorCompletion: { (value) in
                    //self?.resetFields()
                })
            }
        } catch let error {
            self.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}
