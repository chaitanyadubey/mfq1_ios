//
//  ForgotPasswordViewController.swift
//  MyFitQuest
//
//  Created by Imac on 14/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import UIKit
import JGProgressHUD

class ForgotPasswordViewController: UIViewController, HUDProtocol {
    // MARK: - Instance variables
    var instance: JGProgressHUD = JGProgressHUD(style: .extraLight)
    
    @IBOutlet weak var emailField: UITextField!
    
    // MARK: - Life cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    // MARK: - user Defined button Action
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        guard let email = emailField.text, Utilities.isValidEmail(email)  else {
            showAlert(message: "Invalid email")
            return
        }
        print(email)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
