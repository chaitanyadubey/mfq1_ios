//  
//  PersonalInfoViewController.swift
//  MyFitQuest
//  Using Swift 4.0
//
//  Created by Abhishek Singh on 26/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import UIKit
import JGProgressHUD

class PersonalInfoViewController: UIViewController, HUDProtocol, UITextFieldDelegate {
    // MARK: - Instance variables
    var instance: JGProgressHUD = JGProgressHUD(style: .extraLight)
    var currentMember: Member?
    var dobPicker: UIDatePicker?
    
    @IBOutlet weak var dobTextField: UITextField!
    
    // MARK: - Life cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    // MARK: - user Defined button Action

    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
       
    }
    
    func showDatePicker() {
        dobPicker = UIDatePicker()
        dobPicker?.maximumDate = Date()
        dobPicker?.datePickerMode = .date
        dobTextField.inputView = dobPicker
        dobTextField.inputAccessoryView = createPickerToolBar()
        dobTextField.becomeFirstResponder()
    }
    
    func createPickerToolBar() -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneAction))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelAction))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.isTranslucent = false
        toolbar.sizeToFit()
        toolbar.setItems([spaceButton, cancelButton, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        return toolbar
    }
    
    @objc func doneAction() {
        //self.updateDate()
        dobTextField.resignFirstResponder()
        //dobPicker?.removeFromSuperview()
    }
    
    @objc func cancelAction() {
        //self.updateDate()
        dobTextField.resignFirstResponder()
        //dobPicker?.removeFromSuperview()
    }
    
    // MARK: - Text field delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dobTextField {
            showDatePicker()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
