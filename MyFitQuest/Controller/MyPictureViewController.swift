//  
//  MyPictureViewController.swift
//  MyFitQuest
//  Using Swift 4.0
//
//  Created by Abhishek Singh on 22/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import UIKit
import JGProgressHUD
import CropViewController
import TOCropViewController

class MyPictureViewController: UIViewController, HUDProtocol, AddMediaProtocol, TOCropViewControllerDelegate {
    // MARK: - Instance variables
    var instance: JGProgressHUD = JGProgressHUD(style: .extraLight)
    var currentMember: Member?
    var imageSelected = false
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    // MARK: - Life cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        profileImageView.addGestureRecognizer(tap)
        profileImageView.isUserInteractionEnabled = true
        
        if let url = currentMember?.avatar {
            profileImageView.setImageWith(URL: url, placeHolder: nil)
            profileImageView.backgroundColor = UIColor.gray
        } else {
            profileImageView.backgroundColor = UIColor.gray
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    // MARK: - user Defined button Action
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        showAddPhotoDialog(mediaType: .Photo)
    }
    
    @IBAction func backAction(_ sender: Any) {
        hideLoader()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectPhotoAction(_ sender: Any) {
        if imageSelected {
            guard let member = currentMember, let image = profileImageView.image, let data = UIImageJPEGRepresentation(image, 0.2)  else { return }
            showLoader()
            FitQuestAPI.shared.memberAvatarUpload(mid: member.id, data: data, completion: {[weak self] (response) in
                DispatchQueue.main.async {
                    self?.hideLoader()
                    self?.parseAPIResponse(response, type: FQAPIResponse.self, completion: { (resp) in
                        if resp.status == "success" {
                            self?.backAction(sender)
                        }
                    })
                }
            })
        }
    }
    
    // MARK: - AddMediaProtocol delegate
    func didReceiveImage(image: UIImage) {
        profileImageView.image = image
        let cropViewController = TOCropViewController(image: image)
        cropViewController.delegate = self
        
        cropViewController.setAspectRatioPresent(.presetSquare, animated: true)
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.resetAspectRatioEnabled = false
        cropViewController.cropView.cropBoxResizeEnabled = true
        present(cropViewController, animated: true, completion: nil)
    }
   
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        imageSelected = true
        profileImageView.image = image
        cropViewController.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
