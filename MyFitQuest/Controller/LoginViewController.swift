//
//  LoginViewController.swift
//  MyFitQuest
//
//  Created by Imac on 14/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import UIKit
import JGProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController, HUDProtocol, FBProtocol {
    // MARK: - Instance variables
    var instance: JGProgressHUD = JGProgressHUD(style: .extraLight)
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var rememberButton: UIButton!
    
    // MARK: - Life cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        rememberButton.isSelected = UserDetail.remember
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    // MARK: - user Defined button Action
    
    @IBAction func signInAction(_ sender: Any) {
        guard let email = emailField.text, Utilities.isValidEmail(email)  else {
            showAlert(message: "Invalid email")
            return
        }
        guard let password = passwordField.text, password.count > 0 else {
            showAlert(message: "Password can't be blank")
            return
        }
        let loginRequest = LoginRequest(uname: email, password: password)
        emailLogin(requestObject: loginRequest)
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        showLoader()
        fbLogin(withPermissions: ["public_profile", "email", "user_photos","user_location"]) {[weak self] (response) in
            self?.hideLoader()
            switch response {
            case .Success:
                self?.fetchFacebookUserDetails()
            case .Cancelled:
                break
            case .Failed(let error):
                self?.showAlert(message: error.localizedDescription)
            }
        }
    }
    
    func fetchFacebookUserDetails() {
        showLoader()
        fbUserDetails(parameters: ["fields": "name,email,gender,picture"], completion: {[weak self] (response) in
            self?.hideLoader()
            switch response {
            case .Success(let value):
                guard let userDict = value as? [String: Any] else { return }
                print(userDict)
                do {
                    let fbresp = try Utilities.decode(FBUserResponse.self, from: userDict)
                    let nameArr = fbresp.name.split(separator: " ")
                    let fbLoginRequest = FBLoginRequest(email: fbresp.email, token: FBSDKAccessToken.current().tokenString, fbuname: fbresp.name, fname: String(nameArr[0]), lname: String(nameArr[1]), gender: fbresp.gender ?? "", timezone: "-7", manufacturerid: "5")
                    self?.fbLogin(requestObject: fbLoginRequest)
                } catch let error {
                    self?.showAlert(message: error.localizedDescription)
                }
            case .Failed(let message):
                self?.showAlert(message: message)
            }
        })
    }

    @IBAction func rememberMeAction(_ sender: Any) {
        guard let btn = sender as? UIButton else { return }
        btn.isSelected = !btn.isSelected
        UserDetail.remember = btn.isSelected
    }
    
    @IBAction func guestAction(_ sender: Any) {
        
    }
    
    // MARK: - API
    func emailLogin(requestObject: LoginRequest) {
        do {
            guard let paramaters = try Utilities.encode(requestObject) else { return }
            showLoader()
            FitQuestAPI.shared.loginWith(Parameters: paramaters) {[weak self] (response) in
                self?.hideLoader()
                self?.parseAPIResponse(response, type: FQAPIResponse.self, completion: { (resp) in
                    if resp.status == "success" {
                        if let homeViewController = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: "HomeController") as? HomeViewController {
                            homeViewController.memberId = resp.member_id
                            self?.show(homeViewController, sender: self)
                        }
                    }
                })
            }
        } catch let error {
            self.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    func fbLogin(requestObject: FBLoginRequest) {
        do {
            guard let paramaters = try Utilities.encode(requestObject) else { return }
            showLoader()
            FitQuestAPI.shared.fbLoginWith(Parameters: paramaters) {[weak self] (response) in
                self?.hideLoader()
                self?.parseAPIResponse(response, type: FQAPIResponse.self, completion: { (resp) in
                    if resp.status == "success" {
                        if let homeViewController = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: "HomeController") as? HomeViewController {
                            homeViewController.memberId = resp.member_id
                            self?.show(homeViewController, sender: self)
                        }
                    }
                })
            }
        } catch let error {
            self.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}
