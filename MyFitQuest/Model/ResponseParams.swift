//
//  ResponseParams.swift
//  MyFitQuest
//
//  Created by Imac on 18/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import Foundation

struct FQAPIResponse: Codable {
    let status: String
    let member_id: String?
    let message: String?
}

struct FBUserResponse: Codable {
    let name: String
    let email: String
    let gender: String?
    let id: String?
}
