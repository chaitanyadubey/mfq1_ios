//  
//  Country.swift
//  MyFitQuest
//  Using Swift 4.0
//
//  Created by Abhishek Singh on 02/01/18.
//  Copyright © 2018 NGA. All rights reserved.
//

import Foundation

struct CountryList: Codable {
    let countries: [Country]
    private enum CodingKeys: String, CodingKey {
        case countries = "result"
    }
}

struct Country: Codable {
    let name: String
    let id: String
}
