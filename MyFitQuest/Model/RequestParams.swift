//
//  RequestParams.swift
//  MyFitQuest
//
//  Created by Imac on 15/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import Foundation

struct RegisterRequest: Codable {
    let uname: String
    let password: String
    let country: String
    let manufacturerid: String
}

struct LoginRequest: Codable {
    let uname: String
    let password: String
}

struct FBLoginRequest: Codable {
    let email: String
    let token: String
    let fbuname: String
    let fname: String
    let lname: String
    let gender: String
    let timezone: String
    let manufacturerid: String
}

struct AccountDetailUpdate: Codable {
    let mid: String
    let password: String
    let old_password: String
}
