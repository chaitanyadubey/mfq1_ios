//  
//  Member.swift
//  MyFitQuest
//  Using Swift 4.0
//
//  Created by Abhishek Singh on 20/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import Foundation

struct Member :Codable {
    let id: String
    let type: String
    let displayname: String?
    let firstname: String
    let lastname: String
    let email: String
    let city: String?
    let state: String?
    let countryid: String?
    let weight: String?
    let height: String?
    let gender: String?
    let dob: Date?
    let avatar: URL?
    let timezone: String?
    let daylight: String?
    let unit: String?
    let activitylevel: String?
    let fbtoken: String?
    let fbid: String?
    let fbname: String?
    let activate: String?
    let status: String?
    let lastlogin: Date?
    let signupdate: Date?
    let country: String?
    let detail: MemberDetail?
    
    private enum CodingKeys: String, CodingKey {
        case id = "member_id"
        case type = "member_type"
        case displayname = "member_displayname"
        case firstname = "member_firstname"
        case lastname = "member_lastname"
        case email = "member_email"
        case city = "member_city"
        case state = "member_state"
        case countryid = "member_country"
        case weight = "member_weight"
        case height = "member_height"
        case gender = "member_gender"
        case dob = "member_dob"
        case avatar = "member_avatar"
        case timezone = "member_timezone"
        case daylight = "member_daylight"
        case unit = "member_unit"
        case activitylevel = "member_activitylevel"
        case fbtoken = "member_fbtoken"
        case fbid = "member_fbid"
        case fbname = "member_fbname"
        case activate = "member_activate"
        case status = "member_status"
        case lastlogin = "member_lastlogin"
        case signupdate = "member_signupdate"
        case country
        case detail
    }
}

extension Member {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        type = try container.decode(String.self, forKey: .type)
        displayname = try? container.decode(String.self, forKey: .displayname)
        firstname = try container.decode(String.self, forKey: .firstname)
        lastname = try container.decode(String.self, forKey: .lastname)
        email = try container.decode(String.self, forKey: .email)
        city = try? container.decode(String.self, forKey: .city)
        state = try? container.decode(String.self, forKey: .state)
        countryid = try? container.decode(String.self, forKey: .countryid)
        weight = try? container.decode(String.self, forKey: .weight)
        height = try? container.decode(String.self, forKey: .height)
        gender = try? container.decode(String.self, forKey: .gender)
        do {
            dob = try container.decode(Date?.self, forKey: .dob)
        } catch {
            if let dobString = try? container.decode(String.self, forKey: .dob) {
                dob = DateFormatter.yyyyMMdd.date(from: dobString)
            } else {
                dob = nil
            }
        }
        do {
            let avatarString = try container.decode(String.self, forKey: .avatar)
            avatar = URL(string: "\(FitQuestAPI.shared.baseURL)images/profile_image/\(avatarString)")
        } catch {
            avatar = try? container.decode(URL.self, forKey: .avatar)
        }
        timezone = try? container.decode(String.self, forKey: .timezone)
        daylight = try? container.decode(String.self, forKey: .daylight)
        unit = try? container.decode(String.self, forKey: .unit)
        activitylevel = try? container.decode(String.self, forKey: .activitylevel)
        fbtoken = try? container.decode(String.self, forKey: .fbtoken)
        fbid = try? container.decode(String.self, forKey: .fbid)
        fbname = try? container.decode(String.self, forKey: .fbname)
        activate = try? container.decode(String.self, forKey: .activate)
        status = try? container.decode(String.self, forKey: .status)
        lastlogin = try? container.decode(Date.self, forKey: .lastlogin)
        signupdate = try? container.decode(Date.self, forKey: .signupdate)
        country = try? container.decode(String.self, forKey: .country)
        detail = nil
    }
}

extension Member {
    static var current: Member? {
        get {
            return UserDefaults.standard.currentMember
        }
        set {
            UserDefaults.standard.currentMember = newValue
        }
    }
}
