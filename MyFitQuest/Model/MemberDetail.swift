//  
//  MemberDetail.swift
//  MyFitQuest
//  Using Swift 4.0
//
//  Created by Abhishek Singh on 22/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import Foundation

struct MemberDetail: Codable {
    let status: String?
    let total: Int?
    let member_unit: String?
    let name: String?
    let displayname: String?
    let location: String?
    let avatar: String?
    let signupdate: String?
    let bmi: String?
    let bmiresult: String?
    let age: String?
    let heightinft: String?
    let heightininch: String?
    let heightincm: String?
    let height: String?
    let weightinlbs: String?
    let weightinkg: String?
    let weight: String?
    let avghr: String?
    let maxhr: String?
}

extension MemberDetail {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decode(String?.self, forKey: .status)
        total = try container.decode(Int?.self, forKey: .total)
        member_unit = try container.decode(String?.self, forKey: .member_unit)
        name = try container.decode(String?.self, forKey: .name)
        displayname = try container.decode(String?.self, forKey: .displayname)
        location = try container.decode(String?.self, forKey: .location)
        avatar = try container.decode(String?.self, forKey: .avatar)
        signupdate = try container.decode(String?.self, forKey: .signupdate)
        bmi = try container.decode(String?.self, forKey: .bmi)
        bmiresult = try container.decode(String?.self, forKey: .bmiresult)
        age = try container.decode(String?.self, forKey: .age)
        heightinft = try container.decode(String?.self, forKey: .heightinft)
        heightininch = try container.decode(String?.self, forKey: .heightininch)
        heightincm = try container.decode(String?.self, forKey: .heightincm)
        height = try container.decode(String?.self, forKey: .height)
        weightinlbs = try container.decode(String?.self, forKey: .weightinlbs)
        weightinkg = try container.decode(String?.self, forKey: .weightinkg)
        weight = try container.decode(String?.self, forKey: .weight)
        avghr = try container.decode(String?.self, forKey: .avghr)
        maxhr = try container.decode(String?.self, forKey: .maxhr)
    }
}
