//  
//  UserDetail.swift
//  MyFitQuest
//  Using Swift 4.0
//
//  Created by Abhishek Singh on 22/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import Foundation

struct UserDetail: Codable {
    let status: String
    let total: Int
    let member: Member
    
    private enum CodingKeys: String, CodingKey {
        case status
        case total
        case member = "0"
    }
}

extension UserDetail {
    static var remember: Bool {
        get {
            return UserDefaults.standard.rememberLogin
        }
        set {
            UserDefaults.standard.rememberLogin = newValue
        }
    }
}
