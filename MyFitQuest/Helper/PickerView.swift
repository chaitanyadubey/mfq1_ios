import UIKit

typealias DatePickerCompletion = (Date?) -> Void
typealias DatePickerCoundownSelection = (TimeInterval?) -> Void

class DatePickerView: UIView {
    internal var datePicker: UIDatePicker? {
        return _picker
    }
    private var _picker: UIDatePicker?
    var dateSelectionBlock: DatePickerCompletion?
    var countdownSelectionBlock: DatePickerCoundownSelection?
    
    convenience init () {
        self.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 216))
        self.createPicker(mode: .date, max: Date(), min: nil)
        
    }
    
    func createPicker(mode: UIDatePickerMode, max: Date?, min: Date?) {
        _picker = UIDatePicker()
        _picker?.maximumDate = max
        _picker?.minimumDate = min
        _picker?.datePickerMode = mode
    }
    
    func createToolBar(confirmTitle: String = "Done", cancelTitle: String = "Cancel", isCancelAvailable: Bool = true) -> UIToolbar {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: confirmTitle, style: .done, target: self, action: #selector(confirmAction))
        let cancelButton = UIBarButtonItem(title: cancelTitle, style: .done, target: self, action: #selector(cancelAction))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.isTranslucent = false
        toolbar.sizeToFit()
        if isCancelAvailable == false {
            toolbar.setItems([spaceButton, doneButton], animated: false)
        } else {
            toolbar.setItems([spaceButton, cancelButton, doneButton], animated: false)
        }
        toolbar.isUserInteractionEnabled = true
        return toolbar
    }
    
    @objc func confirmAction () {
        if _picker?.datePickerMode == UIDatePickerMode.countDownTimer {
            let interval = _picker?.countDownDuration
            if let selectionBlock = countdownSelectionBlock {
                selectionBlock(interval)
            }
        } else {
            let date = _picker!.date;
            if let selectionBlock = dateSelectionBlock {
                selectionBlock(date)
            }
        }
        cancelAction()
    }
    
    @objc func cancelAction() {
        dateSelectionBlock = nil
        countdownSelectionBlock = nil
    }
    
}


