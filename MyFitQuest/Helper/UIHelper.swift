import UIKit

func showOnWindow(_ controller: UIViewController)  {
    let window = UIWindow(frame: UIScreen.main.bounds)
    window.rootViewController = UIViewController()
    window.windowLevel = UIWindowLevelAlert + 1
    window.makeKeyAndVisible()
    window.rootViewController?.present(controller, animated: true, completion: {
    })
}
