import Foundation

struct Utilities {
    static func getAge (dob: Date) -> Int? {
        let todayDate = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: dob, to: todayDate)
        return ageComponents.year
    }

    static func isValidEmail(_ emailStr:String?) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailStr)
    }
    
    private static var _serverDateFormatter: DateFormatter?
    static func serverDateFormatter() -> DateFormatter? {
        if _serverDateFormatter == nil {
            _serverDateFormatter = DateFormatter()
            _serverDateFormatter?.dateFormat = "yyyy-MM-dd"
        }
        return _serverDateFormatter
    }
    
    static func encode<T>(_ object: T) throws -> [String: Any]? where T : Codable {
        do {
            let encoder = JSONEncoder()
            //encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
            let data = try encoder.encode(object)
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
            guard let params = json as? [String: Any] else {
                return nil
            }
            return params
        } catch let error {
            throw error
        }
    }
    
    static func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Codable {
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.yyyyMMddHHmmss)
            return try decoder.decode(type, from: data)
        } catch let error {
            throw error
        }
    }
    
    static func decode<T>(_ type: T.Type, from json: String) throws -> T where T : Codable {
        do {
            let data = Data(json.utf8)
            return try decode(type, from: data)
        } catch let error {
            throw error
        }
    }
    
    static func decode<T>(_ type: T.Type, from json: [String: Any]) throws -> T where T : Codable {
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return try decode(type, from: data)
        } catch let error {
            throw error
        }
    }
    
}
