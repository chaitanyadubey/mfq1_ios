import UIKit

class MediaHelper: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    static let shared: MediaHelper = MediaHelper()
    
    typealias SucessBlock = (_ picker: UIImagePickerController, _ info: [String : Any]) -> Void
    
    var sucessBlock: SucessBlock?
    var imagePicker: UIImagePickerController?
    
    func imagePickerController(sucessBlock: @escaping SucessBlock) -> UIImagePickerController {
        self.sucessBlock = sucessBlock
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        self.imagePicker = imagePicker
        return imagePicker
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.presentingViewController?.dismiss(animated: true, completion: nil)
        sucessBlock?(picker, info)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
