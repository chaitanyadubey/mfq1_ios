//
//  FitQuestAPI.swift
//  MyFitQuest
//
//  Created by Imac on 14/12/17.
//  Copyright © 2017 NGA. All rights reserved.
//

import Foundation
import Alamofire

public typealias FitQuestCallback = (FitQuestResponse) -> Swift.Void

public enum FitQuestResponse {
    case Success([String: Any])
    case Other([String: Any])
    case Failed(Error)
}

struct FitQuestAPI: APIProtocol {
    static let shared = FitQuestAPI()
    var baseURL: String = "http://mycloudfitness.com/"
    
    //Sampal Call
    private func sample() {
        FitQuestAPI.shared.getMemberDetail(Parameters: ["mid": "10028"]) {(response) in
            switch response {
            case .Success(let value):
                print(value)
            case .Other(let value):
                print(value)
            case .Failed(let message):
                print(message)
            }
        }
    }
    
    private func requestAPI(url: URL, params: [String: Any], method: MethodType = .get, headers: [String: String] = ["Accept": "application/json"], completion: @escaping(FitQuestCallback)) {
        sendAlamofireRequest(url, method: method, parameters: params, headers: headers) { (response, stasusCode, error) in
            guard let responseDict = response as? [String : Any], error == nil else {
                completion(.Failed(error!))
                return
            }
            guard let status = responseDict["status"] as? String, (status == "success" || status == "success-confirmemail") else {
                completion(.Other(responseDict))
                    return
            }
            completion(.Success(responseDict))
        }
    }
    
    func loginWith(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberLogIn.php") else { return }
        requestAPI(url: url, params: params, completion: completion)
    }
    
    func fbLoginWith(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberFBLogIn.php") else { return }
        requestAPI(url: url, params: params, completion: completion)
    }
    
    func registerWith(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberSignUp.php") else { return }
        requestAPI(url: url, params: params, completion: completion)
    }
    
    func countryList(completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"countryList") else { return }
        requestAPI(url: url, params: ["":""], completion: completion)
    }
    
    func getMemberDetail(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"getMemberDetails.php") else { return }
        requestAPI(url: url, params: params, completion: completion)
    }
    
    func getMemberAllDetail(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"getMemberAllDetails.php") else { return }
        requestAPI(url: url, params: params, completion: completion)
    }
    
    func updateMemberDetail(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberDetailsUpdate.php") else { return }
        requestAPI(url: url, params: params, method: .post, completion: completion)
    }
    
    func updateMemberBasicDetail(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberBasicDetailsUpdate.php") else { return }
        requestAPI(url: url, params: params, method: .post, completion: completion)
    }
    
    func updateMemberPhysicalDetail(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberPhysicalDetailsUpdate.php") else { return }
        requestAPI(url: url, params: params, method: .post, completion: completion)
    }
    
    func updateMemberAccountDetail(Parameters params: [String: Any], completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberAccountDetailsUpdate.php") else { return }
        requestAPI(url: url, params: params, method: .post, completion: completion)
    }
    
    func memberAvatarUpload(mid:String, data: Data, completion: @escaping(FitQuestCallback)) {
        guard let url = URL(string: baseURL+"json/"+"memberAvatarUpload.php") else { return }
        let boundary = "---------------------------14737809831466499882746641449"
        let contenttype = "multipart/form-data; boundary=\(boundary)"
        //urlRequest.addValue(contenttype, forHTTPHeaderField: "Content-Type")
        
        let filename = "\(mid)_\(String(Date().timeIntervalSince1970))"
        var body = Data()
        body.append("\r\n--\(boundary)\r\n".utf8data())
        body.append("Content-Disposition: form-data; name=\"avatar\"; filename=\"\(filename).png\"\r\n".utf8data())
        body.append("Content-Type: application/octet-stream\r\n\r\n".utf8data())
        //add image
        body.append(data)
        body.append("\r\n--\(boundary)--\r\n".utf8data())
        body.append("--\(boundary)\r\n".utf8data())
        body.append("Content-Disposition: form-data; name=\"mid\"\r\n\r\n".utf8data())
        body.append(mid.utf8data())
        body.append("\r\n".utf8data())
        body.append("\r\n--\(boundary)--\r\n".utf8data())
        
        sendDataRequest(url, method: .post, body: body, contentType: contenttype, headers: nil) { (response, stasusCode, error) in
            guard let responseDict = response as? [String : Any], error == nil else {
                completion(.Failed(error!))
                return
            }
            completion(.Success(responseDict))
        }
    }
    
}
