import UIKit

typealias CountrySelectionCompletionBlock = (String?, String?) -> Void

open class CountrySelectionInterfaceController: UITableViewController {
    var sectionIndexArray: [String]! = nil
    var alphabetCountryArray: Array<Any>! = nil
    var completionBlock: CountrySelectionCompletionBlock?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "countryCell")
        let countryCodes = Locale.isoRegionCodes
        var countryArray = Array<String>()
        let locale = Locale.current
        for countyCode in countryCodes {
            let countryDisplayName = (locale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countyCode)
            countryArray.append(countryDisplayName!)
        }
        countryArray = countryArray.sorted()
        let alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        alphabetCountryArray = [Any]()
        sectionIndexArray = [String]()
        
        for char in alphabets {
            let alStr = "\(char)"
            let filteredArray = countryArray.filter({(str) -> Bool in
                if String(str[..<str.index(str.startIndex, offsetBy: 1)]) == alStr {
                    return true
                }
                return false
            })
            if filteredArray.count > 0 {
                sectionIndexArray.append(alStr)
                alphabetCountryArray.append(["index": alStr, "items": filteredArray])
            }
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(CountrySelectionInterfaceController.btnCancelTapped(_:)))
        
    }
    
    deinit {
        self.completionBlock = nil
    }
    
    override open var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    override open var prefersStatusBarHidden : Bool {
        return false
    }

    // MARK: - Table view data source
    override open func numberOfSections(in tableView: UITableView) -> Int {
        return self.alphabetCountryArray.count
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dict = self.alphabetCountryArray![section] as! [String: Any]
        return (dict["items"] as! [String]).count
    }

    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath)
        let dict = self.alphabetCountryArray![indexPath.section] as! [String: Any]
        cell.textLabel?.text = (dict["items"] as! [String])[indexPath.row]
        return cell
    }
    
    override open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.sectionIndexArray
    }
    
    override open func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return self.sectionIndexArray.index(of: title)!
    }
    
    // MARK: - UITableViewDelegate
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dict = self.alphabetCountryArray![indexPath.section] as! [String: Any]
        guard let items = dict["items"] as? [String] else {
            completionBlock?(nil, nil)
            return
        }
        if let ids = dict["ids"] as? [String] {
            completionBlock?(items[indexPath.row], ids[indexPath.row])
        } else {
            completionBlock?(items[indexPath.row], nil)
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - IBAction
    @IBAction func btnCancelTapped(_ sender: Any) {
        if let completionBlock = completionBlock {
            completionBlock(nil, nil)
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func sortCustomArray(_ customArray: [Country]) {
        let sortedCountries = customArray.sorted { (country1, country2) -> Bool in
            return country1.name < country2.name
        }
        let alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        alphabetCountryArray = [Any]()
        sectionIndexArray = [String]()
        
        for char in alphabets {
            let alStr = "\(char)"
            let filteredArray = sortedCountries.filter({(country) -> Bool in
                if String(country.name[..<country.name.index(country.name.startIndex, offsetBy: 1)]) == alStr {
                    return true
                }
                return false
            })
            var items:[String] = []
            var ids:[String] = []
            for country in filteredArray {
                items.append(country.name)
                ids.append(country.id)
            }
            if filteredArray.count > 0 {
                sectionIndexArray.append(alStr)
                alphabetCountryArray.append(["index": alStr, "items": items, "ids": ids])
            }
        }
    }

}
