import UIKit
import MobileCoreServices

enum MediaType {
case Photo
case Video
case PhotoAndVideo
    
    var sourceType: [String] {
        switch self {
        case .Photo: return [kUTTypeImage as String]
        case .Video: return [kUTTypeMovie as String]
        case .PhotoAndVideo: return [kUTTypeImage as String, kUTTypeMovie as String]
        }
    }
    
    var takeTitle: String {
        switch self {
        case .Photo: return "Take a photo"
        case .Video: return "Take a video"
        case .PhotoAndVideo: return "Take a photo/video"
        }
    }
    
    var uploadTitle: String {
        switch self {
        case .Photo: return "Upload a photo"
        case .Video: return "Upload a video"
        case .PhotoAndVideo: return "Upload a photo/video"
        }
    }
}

@objc protocol AddMediaProtocol: class {
    @objc optional func didReceiveImage(image: UIImage)
    @objc optional func didReceiveMedia(url: URL)
}

extension AddMediaProtocol where Self: UIViewController {
    func showAddPhotoDialog(mediaType: MediaType,
                            cameraDevice: UIImagePickerControllerCameraDevice = .rear,
                            allowEditing: Bool = false) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: mediaType.takeTitle, style: .default) { [unowned self] action in
            self.takeMedia(mediaType: mediaType, cameraDevice: cameraDevice, allowEditing: allowEditing)
        }
        
        let pickPhoto = UIAlertAction(title: mediaType.uploadTitle, style: .default) { [unowned self] action in
            self.pickMedia(mediaType: mediaType, cameraDevice: cameraDevice, allowEditing: allowEditing)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(takePhoto)
        alert.addAction(pickPhoto)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func takeMedia(mediaType: MediaType,
                   cameraDevice: UIImagePickerControllerCameraDevice = .rear,
                   allowEditing: Bool = false) {
        presentPicker(sourceType: .camera, mediaType: mediaType, cameraDevice: cameraDevice, allowEditing: allowEditing)
    }
    
    func pickMedia(mediaType: MediaType,
                   cameraDevice: UIImagePickerControllerCameraDevice = .rear,
                   allowEditing: Bool = false) {
        presentPicker(sourceType: .photoLibrary, mediaType: mediaType, cameraDevice: nil, allowEditing: allowEditing)
    }
    
    func presentPicker(sourceType: UIImagePickerControllerSourceType,
                       mediaType: MediaType,
                       cameraDevice: UIImagePickerControllerCameraDevice?,
                       allowEditing: Bool = false) {
        
        let imagePickerController = mediaPickerController()
        imagePickerController.allowsEditing = allowEditing
        imagePickerController.sourceType = sourceType
        imagePickerController.mediaTypes = mediaType.sourceType
        
        if let cameraDevice = cameraDevice {
            imagePickerController.cameraDevice = cameraDevice
        }
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func mediaPickerController() -> UIImagePickerController {
        return MediaHelper.shared.imagePickerController(
            sucessBlock: { [unowned self] (picker: UIImagePickerController, info: [String : Any]) -> Void in
                
                guard let mediaType = info[UIImagePickerControllerMediaType] as? String else {
                    return
                }
                
                switch mediaType {
                case kUTTypeImage as String as String:
                    if let image: UIImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                        self.didReceiveImage?(image: image)
                        return
                    }
                    if let image: UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                        self.didReceiveImage?(image: image)
                        return
                    }
                case kUTTypeMovie as String as String:
                    if let mediaURL = info[UIImagePickerControllerMediaURL] as? URL {
                        self.didReceiveMedia?(url: mediaURL)
                        return
                    }
                default: fatalError("unsupported media type please contact developer asap.")
                }
            }
        )
    }
}
