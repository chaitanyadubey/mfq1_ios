import Foundation
import JGProgressHUD

protocol HUDProtocol {
    var instance: JGProgressHUD { get set }
}

extension HUDProtocol where Self: UIViewController {
    func showLoader(With title: String = "") {
        instance.textLabel.text = title
        instance.show(in: view, animated: true)
    }
    
    func hideLoader() {
        instance.dismiss(animated: true)
    }
}
