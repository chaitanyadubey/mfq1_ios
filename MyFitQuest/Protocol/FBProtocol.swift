import Foundation
import FBSDKCoreKit
import FBSDKLoginKit

public typealias FBLoginCallback = (FBLogin) -> Swift.Void
public typealias FBCallback = (FBResponse) -> Swift.Void

public enum FBLogin {
    case Success
    case Cancelled
    case Failed(error: Error)
}

public enum FBResponse {
    case Success(value: Any?)
    case Failed(error: String?)
}

protocol FBProtocol {
    func fbLogin(withPermissions permissions: [String], completion: @escaping(FBLoginCallback))
    func fbUserDetails(parameters: [AnyHashable: Any], completion: @escaping(FBCallback))
}

extension FBProtocol where Self: UIViewController {
    func fbLogin(withPermissions permissions: [String], completion: @escaping(FBLoginCallback)) {
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: permissions, from: self) {(loginResult, error) in
            if error != nil {
                // Error
                completion(FBLogin.Failed(error: error!))
            } else if let cancelled = loginResult?.isCancelled, cancelled == true {
                // Cancelled
                completion(FBLogin.Cancelled)
            } else {
                // Logged in
                completion(FBLogin.Success)
            }
        }
    }
    
    func fbUserDetails(parameters: [AnyHashable: Any], completion: @escaping(FBCallback)) {
        guard let _ = FBSDKAccessToken.current() else {
            completion(FBResponse.Failed(error: "Not logged in"))
            return
        }
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (graphRequestConnection, result, error) in
            if error != nil {
                completion(FBResponse.Failed(error: error?.localizedDescription))
            } else {
                completion(FBResponse.Success(value: result))
            }
        })
    }
}
