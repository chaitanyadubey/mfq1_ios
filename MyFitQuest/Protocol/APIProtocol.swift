import Foundation
import Alamofire

public typealias APIResultCallback = (Any?, Int?, Error?) -> Swift.Void
public enum MethodType: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

protocol APIProtocol {
    //Send Request with Alamofire
    func sendAlamofireRequest(
    _ url: URL,
    method: MethodType,
    parameters: [String: Any]?,
    headers: [String: String]?,
    completion: @escaping(APIResultCallback))
    
    //Send Request with Default Session
    func sendRequest(
    _ url: URL,
    method: MethodType,
    parameters: [String: Any]?,
    headers: [String: String]?,
    completion: @escaping(APIResultCallback))
    
    //Send Request with Default Session and body
    func sendDataRequest(
    _ url: URL,
    method: MethodType,
    body: Data?,
    contentType: String?,
    headers: [String: String]?,
    completion: @escaping(APIResultCallback))
    
}

extension APIProtocol {
    
    // MARK: - API call with Alamofire
    func sendAlamofireRequest(
        _ url: URL,
        method: MethodType = .get,
        parameters: [String: Any]? = nil,
        headers: [String: String]? = nil,
        completion: @escaping(APIResultCallback))
    {
        let httpMethod = HTTPMethod(rawValue: method.rawValue)!
        //Method body
        let request = Alamofire.request(url, method: httpMethod, parameters: parameters, headers: headers)
        request.responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                completion(value, response.response?.statusCode, nil)
            case .failure(let error):
                completion(nil, response.response?.statusCode, error)
            }
        })
    }
    
    // MARK: - API call with URLSession
    func sendRequest(
        _ url: URL,
        method: MethodType = .get,
        parameters: [String: Any]? = nil,
        headers: [String: String]? = nil,
        completion: @escaping(APIResultCallback))
    {
        //Method body 
        var urlRequest:URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        if let param = parameters {
            do {
                let postData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)
                urlRequest.httpBody = postData
                urlRequest.setValue(String(postData.count), forHTTPHeaderField: "Content-Length")
            }
            catch (let error) {
                completion(nil, 600, error)
            }
        }
        //send web request
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            var statusCode: Int?
            var jsonResponse: Any?
            if let httpResponse = response as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            if let dataResponce = data {
                do {
                    jsonResponse = try JSONSerialization.jsonObject(with: dataResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                }
                catch (let error) {
                    completion(nil, 600, error)
                }
            }
            completion(jsonResponse, statusCode, error)
        })
        task.resume()
    }
    
    func sendDataRequest(
        _ url: URL,
        method: MethodType,
        body: Data?,
        contentType: String? = nil,
        headers: [String: String]?,
        completion: @escaping(APIResultCallback))
    {
        //Method body
        var urlRequest:URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.addValue(contentType ?? "", forHTTPHeaderField: "Content-Type")
        urlRequest.allHTTPHeaderFields = headers
        if let body = body {
            urlRequest.httpBody = body
            urlRequest.setValue(String(body.count), forHTTPHeaderField: "Content-Length")
        }
        
        //send web request
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            var statusCode: Int?
            var jsonResponse: Any?
            if let httpResponse = response as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            if let dataResponce = data {
                do {
                    jsonResponse = try JSONSerialization.jsonObject(with: dataResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                }
                catch (let error) {
                    completion(nil, 600, error)
                }
            }
            completion(jsonResponse, statusCode, error)
        })
        task.resume()
    }
    
}
