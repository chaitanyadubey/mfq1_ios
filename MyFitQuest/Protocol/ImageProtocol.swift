import Foundation
import Alamofire
import AlamofireImage

protocol ImageProtocol {
    func loadImage(url: URL, completion: @escaping((UIImage) -> Void))
}

extension ImageProtocol {
    func loadImage(url: URL, completion: @escaping((UIImage) -> Void)) {
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                print("image downloaded: \(image)")
                completion(image)
            }
        }
    }
}
