import Foundation
import UIKit

protocol ShareProtocol: class {
}

extension ShareProtocol where Self: UIViewController {
    func share() {
        let message = "Start making Memories with me! (App download link)"
//        let linkString = "Start making Memories with me! (App download link)"
//        let url = NSURL(string: linkString)
        
        let activityItems: [Any] = [message as Any]
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.airDrop,
                                                        .copyToPasteboard,
                                                        .print,
                                                        .addToReadingList,
                                                        .assignToContact,
                                                        .saveToCameraRoll]
        present(activityViewController, animated: true, completion: nil)
    }
}
