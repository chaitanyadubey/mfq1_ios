import UIKit

extension UIImageView : ImageProtocol {
    func setImageWith(URL url: URL, placeHolder: UIImage?) {
        self.image = placeHolder
        loadImage(url: url) {[weak self] (image) in
            self?.image = image
        }
    }
}
