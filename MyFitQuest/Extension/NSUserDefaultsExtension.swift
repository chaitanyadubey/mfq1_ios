import Foundation

extension UserDefaults {
    
    private struct Keys {
        static let UserKey = "FitFight_User"
        static let Member = "current_member"
        static let RememberMe = "remember_me"
        static let DeviceToken = "device_token"
    }
    
    // Member detail
    var currentMember: Member? {
        get {
            if let data = data(forKey: Keys.Member) {
                do {
                    return try PropertyListDecoder().decode(Member.self, from: data)
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            return nil
        }
        set {
            if let member = newValue {
                do {
                    let data = try PropertyListEncoder().encode(member)
                    set(data, forKey: Keys.Member)
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                removeObject(forKey: Keys.Member)
            }
            synchronize()
        }
    }
    
    var rememberLogin: Bool {
        get {
            return bool(forKey: Keys.RememberMe)
        }
        set {
            set(newValue, forKey: Keys.RememberMe)
            synchronize()
        }
    }
    
    var devicePushToken: String? {
        get {
            return string(forKey: Keys.DeviceToken)
        }
        set {
            if let value = newValue {
                set(value, forKey: Keys.DeviceToken)
            } else {
                removeObject(forKey: Keys.DeviceToken)
            }
            synchronize()
        }
    }
    
}
