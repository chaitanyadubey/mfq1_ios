import UIKit

extension UIAlertController {
    class func getAlert(title: String? = "FitQuest", message: String?, dismissTitle: String? = "Ok", dismissAction: (() -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: dismissTitle, style: .default, handler: { (action) in
            dismissAction?()
        }))
        return alertController
    }
}
