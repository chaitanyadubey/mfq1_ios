import UIKit

public typealias ErrorCompletion = (Any) -> Swift.Void

extension UIViewController {
    func showAlert(title: String? = "FitQuest", message: String?, dismissTitle: String? = "Ok", dismissAction: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: dismissTitle, style: .default, handler: { (action) in
            dismissAction?()
        }))
        present(alertController, animated: true, completion: nil)
    }
    
    //static func decode<T>(_ type: T.Type, from json: [String: Any]) throws -> T where T : Codable {
    func parseAPIResponse<T>(_ response: FitQuestResponse, type: T.Type, completion: @escaping((T) -> Swift.Void), errorCompletion: ErrorCompletion? = nil, isShowAlert: Bool = true) where T : Codable {
        switch response {
        case .Success(let value):
            print(value)
            do {
                let resp = try Utilities.decode(type, from: value)
                completion(resp)
            }
            catch let error {
                if isShowAlert {
                    errorCompletion?(error)
                    showAlert(title: "Error", message: error.localizedDescription)
                } else {
                    errorCompletion?(error)
                }
            }
        case .Other(let value):
            print(value)
            guard let status = value["status"] as? String else {
                do {
                    let resp = try Utilities.decode(type, from: value)
                    completion(resp)
                }
                catch let error {
                    if isShowAlert {
                        errorCompletion?(error)
                        showAlert(title: "Error", message: error.localizedDescription)
                    } else {
                        errorCompletion?(error)
                    }
                }
                return
            }
            if isShowAlert {
                errorCompletion?(value)
                if let message = value["message"] as? String ?? value["msg"] as? String {
                    showAlert(title: "MyFitQuest", message: message)
                } else {
                    showAlert(title: "MyFitQuest", message: status)
                }
            } else {
                errorCompletion?(value)
            }
        case .Failed(let error):
            if isShowAlert {
                errorCompletion?(error)
                showAlert(title: "Error", message: error.localizedDescription)
            } else {
                errorCompletion?(error)
            }
        }
    }
}
