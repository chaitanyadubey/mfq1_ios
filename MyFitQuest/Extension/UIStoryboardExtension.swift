import UIKit

extension UIStoryboard {
    
    fileprivate static var _mainStoryboard: UIStoryboard?
    fileprivate static var _exerciseStoryboard: UIStoryboard?
    fileprivate static var _profileStoryboard: UIStoryboard?
    
    class func mainStoryboard() -> UIStoryboard {
        if _mainStoryboard == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            _mainStoryboard = storyboard
        }
        return _mainStoryboard!
    }
    
    class func exerciseStoryboard() -> UIStoryboard {
        if _exerciseStoryboard == nil {
            let storyboard = UIStoryboard(name: "PaymentStoryboard", bundle: nil)
            _exerciseStoryboard = storyboard
        }
        return _exerciseStoryboard!
    }
    
    class func profileStoryboard() -> UIStoryboard {
        if _profileStoryboard == nil {
            _profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        }
        
        return _profileStoryboard!
    }
}
