import UIKit

extension UIFont {
    private struct FontName {
        static let novecentowideMediumFontName = "Novecentowide-Medium"
        static let robotoThinFontName = "Roboto-Thin"
    }
    
    class func robotoFont(_ size: CGFloat) -> UIFont {
        return UIFont(name: FontName.robotoThinFontName, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func novecentowideFont(_ size: CGFloat) -> UIFont {
        return UIFont(name: FontName.novecentowideMediumFontName, size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
